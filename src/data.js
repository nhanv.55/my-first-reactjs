export const scientistList = [
  {
    id: "u001",
    name: "Katherine Johnson",
    imageId: "MK3eW3As",
    profession: "mathematician",
    bio: "Although Colvin is predominantly known for abstract themes that allude to pre-Hispanic symbols, this gigantic sculpture, an homage to neurosurgery, is one of her most recognizable public art pieces.",
  },
  {
    id: "u002",
    name: "Katsuko Saruhashi",
    imageId: "YfeOqp2",
    profession: "physicist",
    bio: "The Terracotta Army is a collection of terracotta sculptures depicting the armies of Qin Shi Huang, the first Emperor of China. The army consisted of more than 8,000 soldiers, 130 chariots with 520 horses, and 150 cavalry horses.",
  },
  {
    id: "u003",
    name: "Aklilu Lemma",
    imageId: "OKS67lh",
    profession: "astrophysicist",
    bio: "This enormous (75 ft. or 23m) silver flower is located in Buenos Aires. It is designed to move, closing its petals in the evening or when strong winds blow and opening them in the morning.",
  },
  {
    id: "u004",
    name: "Lin Lanying",
    imageId: "1bX5QH6",
    profession: "mathematician",
    bio: 'Wilson was known for his preoccupation with equality, social justice, as well as the essential and spiritual qualities of humankind. This massive (7ft. or 2,13m) bronze represents what he described as "a symbolic Black presence infused with a sense of universal humanity."',
  },
  {
    id: "u005",
    name: "Creola Katherine Johnson",
    imageId: "MK3eW3A",
    profession: "physicist",
    bio: "Located on the Easter Island, there are 1,000 moai, or extant monumental statues, created by the early Rapa Nui people, which some believe represented deified ancestors.",
  },
  {
    id: "u006",
    name: "Mario José Molina-Pasquel Henríquez",
    imageId: "mynHUSa",
    profession: "chemist",
    bio: "The Nanas are triumphant creatures, symbols of femininity and maternity. Initially, Saint Phalle used fabric and found objects for the Nanas, and later on introduced polyester to achieve a more vibrant effect.",
  },
  {
    id: "u007",
    name: "Mohammad Abdus Salam",
    imageId: "bE7W1ji",
    profession: "astrophysicist",
    bio: "This abstract bronze sculpture is a part of The Family of Man series located at Yorkshire Sculpture Park. Hepworth chose not to create literal representations of the world but developed abstract forms inspired by people and landscapes.",
  },
  {
    id: "u008",
    name: "Percy Lavon Julian",
    imageId: "IOjWm71",
    profession: "mathematician",
    bio: "Descended from four generations of woodcarvers, Fakeye's work blended traditional and contemporary Yoruba themes.",
  },
  {
    id: "u009",
    name: "Subrahmanyan Chandrasekhar",
    imageId: "lrWQx8l",
    profession: "chemist",
    bio: "Szapocznikow is known for her sculptures of the fragmented body as a metaphor for the fragility and impermanence of youth and beauty. This sculpture depicts two very realistic large bellies stacked on top of each other, each around five feet (1,5m) tall.",
  },
];
