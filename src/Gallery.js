import { Avatar, DisplayType } from "./Avatar";
import { scientistList } from "./data.js";

function PersonalItem({ person, onItemClick }) {
  return (
    person != null && (
      <div className="profile-container" onClick={() => onItemClick(person)}>
        <div className="avatar-container">
          <Avatar
            imageId={person.imageId}
            description={person.name}
            displayType={DisplayType.AVATAR}
            size={80}
          />
        </div>
        <div className="profile-info">
          <h1 className="name">{person.name}</h1>
          <h3 className="bio">{person.profession} profestional</h3>
        </div>
      </div>
    )
  );
}

export default function Gallery({ onItemClick }) {
  return (
    <section>
      <h1>Amazing scientists</h1>
      <div>
        {scientistList.map((p) => (
          <PersonalItem key={p.id} person={p} onItemClick={onItemClick} />
        ))}
      </div>
    </section>
  );
}
