import logo from "./logo.svg";
import "./App.css";
import { Profile } from "./Profile";
import Gallery from "./Gallery";
import { useState } from "react";
import { scientistList } from "./data";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

function MyApp() {
  const [selectedUser, setSelectedUser] = useState(scientistList[0]);

  return (
    <div style={{ padding: "20px" }}>
      {/* <App /> */}
      {/* <Profile
        person={{
          name: "Katherine Johnson",
          imageId: "",
        }}
      /> */}
      <Profile person={selectedUser} />
      <Gallery
        onItemClick={(p) => {
          setSelectedUser(p);
        }}
      />
    </div>
  );
}

export default MyApp;
