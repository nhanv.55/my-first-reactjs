const baseUrl = "https://i.imgur.com/";

export const DisplayType = {
  INFO: "info",
  AVATAR: "avatar",
};

export function Avatar({
  imageId,
  description,
  displayType = DisplayType.INFO,
  size = 100,
}) {
  return (
    <img
      className={
        displayType === DisplayType.AVATAR ? "avatar" : "image-gray-border"
      }
      src={baseUrl + imageId + ".jpg"}
      alt={description}
      width={size}
      height={size}
    />
  );
}
