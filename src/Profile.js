import { Avatar } from "./Avatar";

export function Profile({ person }) {
  return (
    person != null && (
      <>
        <h1>Hi {person.name} </h1>
        {person.bio != null && (
          <p style={{ fontSize: "14px", fontStyle: "italic" }}>{person.bio}</p>
        )}
        <Avatar imageId={person.imageId} description={person.name} />
      </>
    )
  );
}
